package com.example.digitalworks.newsapp.MainActivity.RetroFitApi;

import com.example.digitalworks.newsapp.MainActivity.Models.NewsResponse;
import com.example.digitalworks.newsapp.MainActivity.Models.SourcesResponse;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface API {

    @GET("sources")
    Call<SourcesResponse> getSources(@Query("apiKey") String apiKey);

    @GET("everything")
    Call<NewsResponse> getNews(@Query("apiKey") String apiKey , @Query("sources") String source);
}
