package com.example.digitalworks.newsapp.MainActivity.Main2Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.example.digitalworks.newsapp.MainActivity.Models.ArticlesItem;
import com.example.digitalworks.newsapp.R;

import java.util.ArrayList;
import java.util.List;

public class Main2Activity extends AppCompatActivity {
    private static final String TAG = "Main2Activity";

    //widgets
    RecyclerView recyclerView;
    NewsAdapter adapter;
    List<ArticlesItem> mArticles = new ArrayList<>();
    NewsData newsData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        init();
        getData();
    }

    void init(){
        recyclerView = findViewById(R.id.recyclerView);
        adapter = new NewsAdapter(mArticles,getApplicationContext());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
    }

    void getData(){
        newsData = new NewsData(getApplicationContext(), new iNewsData() {
            @Override
            public void onArticlesFinished(List<ArticlesItem> articles) {
                mArticles.addAll(articles);
                adapter.notifyDataSetChanged();

            }

            @Override
            public void onError(Throwable t) {
                Log.e(TAG, "onError: "+t.getMessage());

            }
        });
        newsData.getNews(getIntent().getStringExtra("id"));

    }
}
