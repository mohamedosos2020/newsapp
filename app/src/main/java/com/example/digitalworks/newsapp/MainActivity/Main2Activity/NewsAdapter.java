package com.example.digitalworks.newsapp.MainActivity.Main2Activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.view.NestedScrollingChild;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.digitalworks.newsapp.MainActivity.Models.ArticlesItem;
import com.example.digitalworks.newsapp.R;

import java.util.List;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ViewHolder> {
    private List<ArticlesItem>  articles;
    private Context context;

    public NewsAdapter(List<ArticlesItem>  articles , Context context){
        this.articles = articles;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(context).inflate(R.layout.item_news_article,parent,false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final ArticlesItem article = articles.get(position);
        holder.sourceNameTxtView.setText(article.getSource().getName());
        holder.articleTitleTxtView.setText(article.getTitle());
        Glide.with(context).load(article.getUrlToImage()).into(holder.articleImageView);
        holder.articleDescTxtView.setText(article.getDescription());

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(article.getUrl()));
                context.startActivity(i);
            }
        });

    }

    @Override
    public int getItemCount() {
         return articles != null ? articles.size() : 0;

    }


    public static class ViewHolder extends RecyclerView.ViewHolder{

        private CardView cardView;
        private TextView sourceNameTxtView;
        private TextView articleTitleTxtView;
        private ImageView articleImageView;
        private TextView articleDescTxtView;


        public ViewHolder(View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.cardView_item);
            sourceNameTxtView = itemView.findViewById(R.id.sourceNameTxtView);
            articleTitleTxtView = itemView.findViewById(R.id.articleTitleTxtView);
            articleImageView = itemView.findViewById(R.id.articleImageView);
            articleDescTxtView = itemView.findViewById(R.id.articleDescTxtView);

        }
    }
}