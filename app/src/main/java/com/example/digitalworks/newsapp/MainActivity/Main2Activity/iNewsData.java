package com.example.digitalworks.newsapp.MainActivity.Main2Activity;

import com.example.digitalworks.newsapp.MainActivity.Models.ArticlesItem;
import com.example.digitalworks.newsapp.MainActivity.Models.SourcesItem;

import java.util.List;

public interface iNewsData {

    abstract void onArticlesFinished(List<ArticlesItem> articles);

    abstract void onError(Throwable t);

}
