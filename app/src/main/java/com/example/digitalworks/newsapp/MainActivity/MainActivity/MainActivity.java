package com.example.digitalworks.newsapp.MainActivity.MainActivity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.example.digitalworks.newsapp.MainActivity.Models.SourcesItem;
import com.example.digitalworks.newsapp.R;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    //widgets
    RecyclerView recyclerView;
    SourcesData SourcesData;
    SourcesAdapter adapter;
    List<SourcesItem> sourcesList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();

    }

    void init() {
        recyclerView = findViewById(R.id.recyclerView);

        adapter = new SourcesAdapter(sourcesList, getApplicationContext());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        getData();
    }

    void getData() {
        SourcesData = new SourcesData(getApplicationContext(), new iSourcesData() {
            @Override
            public void onSourcesFinished(List<SourcesItem> sources) {
                sourcesList.addAll(sources);
                adapter.notifyDataSetChanged();

            }

            @Override
            public void onError(Throwable t) {
                Log.e(TAG, "onError: " + t.getMessage());
            }
        });
        SourcesData.getSources();
    }
}
