package com.example.digitalworks.newsapp.MainActivity.MainActivity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.digitalworks.newsapp.MainActivity.Main2Activity.Main2Activity;
import com.example.digitalworks.newsapp.MainActivity.Models.SourcesItem;
import com.example.digitalworks.newsapp.R;

import java.util.List;

public class SourcesAdapter extends RecyclerView.Adapter<SourcesAdapter.ViewHolder> {
    private List<SourcesItem>  sources;
    private Context context;

    public SourcesAdapter(List<SourcesItem>  sources ,Context context){
        this.sources = sources;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(context).inflate(R.layout.item_news_source,parent,false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final SourcesItem source = sources.get(position);
        holder.sourceTxtView.setText(source.getName());
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =new Intent(context,Main2Activity.class);
                intent.putExtra("id",source.getId());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return sources != null ? sources.size() : 0;

    }


    public static class ViewHolder extends RecyclerView.ViewHolder{

        CardView cardView;
        private TextView sourceTxtView;
        private TextView goTxtView;

        public ViewHolder(View itemView) {
            super(itemView);
            sourceTxtView = itemView.findViewById(R.id.sourceTxtView);
            cardView = itemView.findViewById(R.id.cardView_item);
            goTxtView = itemView.findViewById(R.id.goTextView);

        }
    }
}