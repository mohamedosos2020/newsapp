package com.example.digitalworks.newsapp.MainActivity.MainActivity;

import com.example.digitalworks.newsapp.MainActivity.Models.ArticlesItem;
import com.example.digitalworks.newsapp.MainActivity.Models.SourcesItem;

import java.util.List;

public interface iSourcesData {

    abstract void onSourcesFinished(List<SourcesItem> sources);

    abstract void onError(Throwable t);

}
