package com.example.digitalworks.newsapp.MainActivity.RetroFitApi;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetroFitManager {

    private static Retrofit retrofit;
    private static Retrofit getInstance(){
        if (retrofit == null){
            retrofit = new Retrofit
                    .Builder()
                    .baseUrl("https://newsapi.org/v2/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public static API getApi(){
        return getInstance().create(API.class);
    }
}
