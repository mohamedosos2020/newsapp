package com.example.digitalworks.newsapp.MainActivity.Main2Activity;

import android.content.Context;
import android.util.Log;

import com.example.digitalworks.newsapp.MainActivity.MainActivity.iSourcesData;
import com.example.digitalworks.newsapp.MainActivity.Models.ArticlesItem;
import com.example.digitalworks.newsapp.MainActivity.Models.NewsResponse;
import com.example.digitalworks.newsapp.MainActivity.Models.SourcesItem;
import com.example.digitalworks.newsapp.MainActivity.Models.SourcesResponse;
import com.example.digitalworks.newsapp.MainActivity.RetroFitApi.RetroFitManager;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class NewsData {
    private static final String TAG = "SourcesData";

    private Context context;
    private List<ArticlesItem> articles = new ArrayList<>();
    iNewsData iNewsData;


    public NewsData(Context context, iNewsData iNewsData) {
        this.context = context;
        this.iNewsData = iNewsData;
    }

    public List<ArticlesItem> getNews(String sourceName) {
        RetroFitManager.getApi().getNews("f2fb04edc9ed473f844bce5dc596e024", sourceName).enqueue(new Callback<NewsResponse>() {
            @Override
            public void onResponse(Call<NewsResponse> call, Response<NewsResponse> response) {
                NewsResponse newsResponse = response.body();
                articles = newsResponse.getArticles();
                iNewsData.onArticlesFinished(articles);
                Log.e(TAG, "onResponse: "+articles.toString());
            }

            @Override
            public void onFailure(Call<NewsResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: "+t.getMessage());
                iNewsData.onError(t);


            }
        });

        return articles;
    }
}
