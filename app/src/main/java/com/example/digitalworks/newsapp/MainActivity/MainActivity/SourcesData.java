package com.example.digitalworks.newsapp.MainActivity.MainActivity;

import android.content.Context;
import android.util.Log;

import com.example.digitalworks.newsapp.MainActivity.Models.SourcesItem;
import com.example.digitalworks.newsapp.MainActivity.Models.SourcesResponse;
import com.example.digitalworks.newsapp.MainActivity.RetroFitApi.RetroFitManager;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SourcesData {
    private static final String TAG = "SourcesData";

    private Context context;
    private List<SourcesItem> sources;
    iSourcesData iSourcesData;


    public SourcesData(Context context, iSourcesData iNewsData) {
        this.context = context;
        this.iSourcesData = iNewsData;
    }

    public List<SourcesItem> getSources() {
        RetroFitManager.getApi().getSources("f2fb04edc9ed473f844bce5dc596e024").enqueue(new Callback<SourcesResponse>() {
            @Override
            public void onResponse(Call<SourcesResponse> call, Response<SourcesResponse> response) {
                SourcesResponse sourcesResponse = response.body();
                sources = sourcesResponse.getSources();
                Log.e(TAG, "onResponse: "+sources.toString());
                iSourcesData.onSourcesFinished(sources);
            }

            @Override
            public void onFailure(Call<SourcesResponse> call, Throwable t) {
                iSourcesData.onError(t);
                Log.e(TAG, "onFailure: "+t.getMessage() );
            }
        });

        return sources;
    }
}
